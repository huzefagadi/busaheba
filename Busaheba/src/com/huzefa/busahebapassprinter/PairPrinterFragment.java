package com.huzefa.busahebapassprinter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.aem.api.AEMScrybeDevice;
import com.aem.api.IAemScrybe;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mufaddalgulshan on 16/09/14.
 */
public class PairPrinterFragment extends Fragment {


    public static final String TAG = "PairPrinterFragment";
    private Button btnPairConnect;
    private Button btnLater;
    private PairPrinterCallback mPairPrinterCallback;
    private Toast mToast = null;
    private AEMScrybeDevice mAEMDevice;
    private AlertDialog.Builder mDialog;
    private ArrayAdapter<String> mAdapter;
    private String mAEMPrinterName;
    private ArrayList<String> mAEMPrinterList;
    private Bundle mBundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pair_printer_fragment, container, false);
        if (v != null) {

            mBundle = this.getArguments();
            mAEMDevice = new AEMScrybeDevice(new IAemScrybe() {
                @Override
                public void onDiscoveryComplete(ArrayList<String> mPrinterList) {

                }
            });

            btnPairConnect = (Button) v.findViewById(R.id.btnPair);
            btnLater = (Button) v.findViewById(R.id.btnLater);

            if (mBundle.getString("ACTION") != null) {
                btnPairConnect.setText(mBundle.getString("ACTION"));
            }

            if (btnPairConnect.getText().toString().equals("Pair")) {
                btnLater.setVisibility(View.VISIBLE);
            } else {
                btnLater.setVisibility(View.GONE);
            }

            btnPairConnect.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        btnPairConnect.setBackgroundColor(getResources().getColor(R.color.Button_Pressed));
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        btnPairConnect.setBackgroundColor(getResources().getColor(R.color.Button));
                    }
                    return false;
                }
            });

            btnPairConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (!mBluetoothAdapter.isEnabled()) {
                        mBluetoothAdapter.enable();
                    }

                    //AEM
                    if (SettingsHelper.getPrinterSetting(getActivity()).equals("AEM")) {

                        mAEMPrinterList = mAEMDevice.getPairedPrinters();

                        if (mAEMPrinterList.size() > 0) {

                            mDialog = new AlertDialog.Builder(getActivity());
                            mDialog.setTitle("Pick a device");
                            mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1);
                            for (int i = 0; i < mAEMPrinterList.size(); i++) {
                                mAdapter.add(mAEMPrinterList.get(i));
                            }
                            mDialog.setAdapter(mAdapter, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mAEMPrinterName = mAdapter.getItem(i).toString();

                                    try {
                                        if (mAEMDevice.connectToPrinter(mAEMPrinterName)) {
                                            mPairPrinterCallback.onPrinterConnected(mAEMPrinterName, mAEMDevice);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        showToast("Unable to connect to " + mAEMPrinterName);
                                    }


                                }
                            });
                            mDialog.create();
                            mDialog.show();
                        } else {
                            Intent mPairPrinterIntent = new Intent(Settings.ACTION_BLUETOOTH_SETTINGS);
                            startActivity(mPairPrinterIntent);
                        }
                    }

                    //ZEBRA
                    else if (SettingsHelper.getPrinterSetting(getActivity()).equals("Zebra")) {
                        Intent mPairZebraIntent = new Intent(getActivity(), PairZebraPrinterActivity.class);
                        startActivityForResult(mPairZebraIntent, 1);
                    }
                }
            });

            btnLater.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        btnLater.setBackgroundColor(getResources().getColor(R.color.Button_Pressed));
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        btnLater.setBackgroundColor(getResources().getColor(R.color.Button));
                    }
                    return false;
                }
            });

            btnLater.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPairPrinterCallback.onLater();
                }
            });
        }
        return v;
    }

    public interface PairPrinterCallback {
        public void onPrinterConnected(String mPrinterName, AEMScrybeDevice mDevice);

        public void onPrinterPaired(String zebraPrinterBluetoothAddress);

        public void onLater();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof PairPrinterCallback) {
            mPairPrinterCallback = (PairPrinterCallback) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement PairPrinterCallback");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == getActivity().RESULT_OK) {
                if (data.hasExtra("PAIRED")) {
                    mPairPrinterCallback.onPrinterPaired(data.getStringExtra("PAIRED"));
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mBundle.getString("ACTION", "Connect").equals("Connect")) {
            if (SettingsHelper.getPrinterSetting(getActivity()).equals("AEM")) {
                ArrayList<String> mPairedPrintersList = mAEMDevice.getPairedPrinters();
                if (mPairedPrintersList.size() != 0) {
                    mPairPrinterCallback.onPrinterPaired("");
                }
            } else if (SettingsHelper.getPrinterSetting(getActivity()).equals("Zebra")) {
                if (SettingsHelper.getBluetoothAddress(getActivity()) != "") {
                    mPairPrinterCallback.onPrinterPaired(SettingsHelper.getBluetoothAddress(getActivity()));
                }
            }
        }
    }

    private void showToast(String strMessage) {
        if (mToast == null) {
            mToast = Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT);
        }
        mToast.setText(strMessage);
        mToast.show();
    }
}
