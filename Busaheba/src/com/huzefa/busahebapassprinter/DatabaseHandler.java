package com.huzefa.busahebapassprinter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 2;

	// Database Name
	private static final String DATABASE_NAME = "BUSAHEBA";
	private Context context;
	private static final String TABLE_NAME_1 = "TABLE_PASSES";
	private static final String TABLE_NAME_2 = "TABLE_PASSES_HISTORY";
	private static final String TABLE_NAME_3 = "TABLE_IN_DATA";
	private static final String TABLE_NAME_4 = "TABLE_TAG_ID_IN_DATA";
	private static final String ID = "ID";
	private static final String TAGID = "TAGID";
	
	SimpleDateFormat sm = new SimpleDateFormat("MM-dd-yyyy");
	private static final String ITS="ITS";
	private static final String IN="INDATA";
	private static final String DATEANDTIME="DATEANDTIME";
	File directory= new File(Environment.getExternalStorageDirectory()+"/BUSAHEBA");
	File directoryForTagId= new File(Environment.getExternalStorageDirectory()+"/BUSAHEBATAGID");

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context=context;
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_TABLE_1 = "CREATE TABLE " + TABLE_NAME_1 +
				"(" + ID + " INTEGER PRIMARY KEY  AUTOINCREMENT" +
				", NAME VARCHAR(1000)" +
				", ITS_ID VARCHAR(10)" +
				", TAG_ID VARCHAR(50)" +
				", CHILD VARCHAR(1)" +
				", CHILD_ITS VARCHAR(10)" +
				", VENUE VARCHAR(200)" +
				", GATE VARCHAR(10)" +
				", FLOOR VARCHAR(10)" +
				", BLOCK VARCHAR(10)" +
				", SEAT VARCHAR(10))";


		String CREATE_TABLE_2 = "CREATE TABLE " + TABLE_NAME_2 + "(" + ID
				+ " INTEGER PRIMARY KEY  AUTOINCREMENT, ITS VARCHAR(10), DAY INTEGER, MONTH VARCHAR(50))";

		String CREATE_TABLE_3 ="CREATE TABLE "+TABLE_NAME_3 + "(" + ITS +" VARCHAR(10) PRIMARY KEY,"+IN+" VARCHAR(2), "+DATEANDTIME+" VARCHAR(20))";
		String CREATE_TABLE_4 ="CREATE TABLE "+TABLE_NAME_4 + "(" + TAGID +" VARCHAR(30) PRIMARY KEY,"+IN+" VARCHAR(2), "+DATEANDTIME+" VARCHAR(20))";

		db.execSQL(CREATE_TABLE_1);
		db.execSQL(CREATE_TABLE_2);
		db.execSQL(CREATE_TABLE_3);
		db.execSQL(CREATE_TABLE_4);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_1);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_2);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_3);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_4);

		// Create tables again
		onCreate(db);
	}

	public boolean rowExists() {
		boolean bIsValid = false;
		SQLiteDatabase db = this.getReadableDatabase();

		String sql = "SELECT ID from " + TABLE_NAME_1;

		Cursor mCur = db.rawQuery(sql, null);
		if (mCur != null) {
			if (mCur.getCount() > 0) {
				bIsValid = true;
				return bIsValid;
			}

		}
		return bIsValid;
	}

	long getPrintedPassesCount() {
		SQLiteDatabase db = this.getReadableDatabase();

		String sql = "SELECT COUNT(*) from " + TABLE_NAME_2;

		Cursor mCur = db.rawQuery(sql, null);
		if (mCur != null) {
			mCur.moveToNext();
		}
		return mCur.getInt(0);
	}

	int deletePasses() {
		int iReturn = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME_1 + "'");
		iReturn = db.delete(TABLE_NAME_1, "1", null);
		db.close();
		return iReturn;
	}

	int deleteHistory() {
		int iReturn = 0;
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '" + TABLE_NAME_2
				+ "'");
		iReturn = db.delete(TABLE_NAME_2, "1", null);
		db.close();
		return iReturn;
	}

	public long insertItems(String mName, String mITS, String mTagId, String mChild, String mChildITS,
			String mVenue, String mGate, String mFLoor, String mBlock, String mSeat) {
		SQLiteDatabase db = this.getWritableDatabase();
		long iError = 0;
		ContentValues values = new ContentValues();
		values.put("NAME", mName);
		values.put("ITS_ID", mITS);
		values.put("TAG_ID", mTagId);
		values.put("CHILD", mChild);
		values.put("CHILD_ITS", mChildITS);
		values.put("VENUE", mVenue);
		values.put("GATE", mGate);
		values.put("FLOOR", mFLoor);
		values.put("BLOCK", mBlock);
		values.put("SEAT", mSeat);

		// Inserting Row
		iError = db.insert(TABLE_NAME_1, null, values);
		db.close(); // Closing database connection
		return iError;
	}

	public Cursor getPassDetailsByITS(String strITS) {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT " +
				" NAME " +
				",ITS_ID " +
				",TAG_ID " +
				",CHILD " +
				",CHILD_ITS " +
				",VENUE " +
				",GATE " +
				",FLOOR " +
				",BLOCK " +
				",SEAT " +
				" FROM " + TABLE_NAME_1 +
				" WHERE ITS_ID = '" + strITS + "'";

		Cursor mCur = db.rawQuery(sql, null);
		if (mCur != null) {
			mCur.moveToNext();
		}

		return mCur;
	}

	public Cursor getMemberDetailsByTagId(String strTagId) {
		SQLiteDatabase db = this.getReadableDatabase();
		String sql = "SELECT " +
				" NAME " +
				",ITS_ID " +
				",TAG_ID " +
				",CHILD " +
				",CHILD_ITS " +
				",VENUE " +
				",GATE " +
				",FLOOR " +
				",BLOCK " +
				",SEAT " +
				" FROM " + TABLE_NAME_1 +
				" WHERE UPPER(TAG_ID) = '" + strTagId + "'";

		Cursor mCur = db.rawQuery(sql, null);
		if (mCur != null) {
			mCur.moveToNext();
		}

		return mCur;
	}

	public long insertINData(String its,String in,String dateAndTime )
	{
		SQLiteDatabase db = this.getWritableDatabase();
		long iError = 0;
		ContentValues values = new ContentValues();
		values.put(ITS, its);
		values.put(IN, in);
		values.put(DATEANDTIME, dateAndTime);

		// Inserting Row
		try {
			iError = db.insert(TABLE_NAME_3, null, values);
		}

		catch (SQLiteConstraintException e)
		{
			System.out.println("duplicate data");
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		db.close(); // Closing database connection
		return iError;
	}

	public long exportInData()
	{
		//+
		File fileSuccess =new File(directory,"PRINT_"+String.valueOf(sm.format(new Date()))+".csv");
		StringBuilder str = new StringBuilder();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME_3;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		long totalRecords = c.getCount();
		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				str.append(c.getString((c.getColumnIndex(ITS)))).append(",");
				str.append((c.getString(c.getColumnIndex(IN)))).append(",");
				str.append((c.getString(c.getColumnIndex(DATEANDTIME)))).append("\n");
				// adding to todo list

			} while (c.moveToNext());
		}
		FileOutputStream fOut;
		try {
			if(!directory.exists())
			{
				directory.mkdir();
			}
			if(!fileSuccess.exists())
			{
				fileSuccess.createNewFile();
			}
			fOut = new FileOutputStream(fileSuccess,true);
			//fOut = ctx.openFileOutput(fileSuccess.getAbsolutePath(), Context.MODE_APPEND);
			OutputStreamWriter myOutWriter = 
					new OutputStreamWriter(fOut);
			myOutWriter.append(str);
			myOutWriter.close();
			fOut.close();
			System.out.println("DELETED DATA"+deleteINDATA());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return totalRecords;

	}
	int deleteINDATA() {
		SQLiteDatabase db= this.getWritableDatabase();
		int rowCount = db.delete(TABLE_NAME_3,
		                    null,
		                    null);
		 return rowCount;
	}
	public long insertInHistory(String mITS, String mDay, String mMonth) {
		SQLiteDatabase db = this.getWritableDatabase();
		long iError = 0;
		ContentValues values = new ContentValues();
		values.put("ITS", mITS);
		values.put("DAY", mDay);
		values.put("MONTH", mMonth);

		// Inserting Row
		iError = db.insert(TABLE_NAME_2, null, values);
		db.close(); // Closing database connection
		return iError;
	}
	/*
	 * INDATA WITH TAG ID
	 * 
	 * 
	 */
	int deleteTagIdINDATA() {
		SQLiteDatabase db= this.getWritableDatabase();
		int rowCount = db.delete(TABLE_NAME_4,
		                    null,
		                    null);
		 return rowCount;
	}
	public long insertTagIdINData(String its,String in,String dateAndTime )
	{
		SQLiteDatabase db = this.getWritableDatabase();
		long iError = 0;
		ContentValues values = new ContentValues();
		values.put(TAGID, its);
		values.put(IN, in);
		values.put(DATEANDTIME, dateAndTime);

		// Inserting Row
		try {
			iError = db.insert(TABLE_NAME_4, null, values);
		}

		catch (SQLiteConstraintException e)
		{
			System.out.println("duplicate data");
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			UIHelper.showToast("THIS ID HAS BEEN SCANNED BEFORE!!", context);
			e.printStackTrace();
		}
		db.close(); // Closing database connection
		return iError;
	}

	public long exportTagIdInData()
	{
		//+
		File fileSuccess =new File(directoryForTagId,"MARKIN_"+String.valueOf(sm.format(new Date()))+".csv");
		StringBuilder str = new StringBuilder();
		String selectQuery = "SELECT  * FROM " + TABLE_NAME_4;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		long totalRecords = c.getCount();
		System.out.println(totalRecords);
		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				str.append(c.getString((c.getColumnIndex(TAGID)))).append(",");
				str.append((c.getString(c.getColumnIndex(IN)))).append(",");
				str.append((c.getString(c.getColumnIndex(DATEANDTIME)))).append("\n");
				// adding to todo list

			} while (c.moveToNext());
		}
		FileOutputStream fOut;
		try {
			if(!directoryForTagId.exists())
			{
				directoryForTagId.mkdir();
			}
			if(!fileSuccess.exists())
			{
				fileSuccess.createNewFile();
			}
			fOut = new FileOutputStream(fileSuccess,true);
			//fOut = ctx.openFileOutput(fileSuccess.getAbsolutePath(), Context.MODE_APPEND);
			OutputStreamWriter myOutWriter = 
					new OutputStreamWriter(fOut);
			myOutWriter.append(str);
			myOutWriter.close();
			fOut.close();
			System.out.println("DELETED TAGID DATA"+deleteTagIdINDATA());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
		
		
		
		return totalRecords;

	}
	 
}