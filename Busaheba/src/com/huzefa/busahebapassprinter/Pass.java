package com.huzefa.busahebapassprinter;

public class Pass {

    private String Name;
    private String ITSId;
    private String TagId;
    private String Child;
    private String ChildITS;
    private String Venue;
    private String Gate;
    private String Floor;
    private String Block;
    private String Seat;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getITSId() {
        return ITSId;
    }

    public void setITSId(String ITSId) {
        this.ITSId = ITSId;
    }

    public String getTagId() {
        return TagId;
    }

    public void setTagId(String tagId) {
        TagId = tagId;
    }

    public String getChild() {
        return Child;
    }

    public void setChild(String child) {
        Child = child;
    }

    public String getChildITS() {
        return ChildITS;
    }

    public void setChildITS(String childITS) {
        ChildITS = childITS;
    }

    public String getVenue() {
        return Venue;
    }

    public void setVenue(String venue) {
        Venue = venue;
    }

    public String getGate() {
        return Gate;
    }

    public void setGate(String gate) {
        Gate = gate;
    }

    public String getFloor() {
        return Floor;
    }

    public void setFloor(String floor) {
        Floor = floor;
    }

    public String getBlock() {
        return Block;
    }

    public void setBlock(String block) {
        Block = block;
    }

    public String getSeat() {
        return Seat;
    }

    public void setSeat(String seat) {
        Seat = seat;
    }

    public Pass(String name, String ITSId, String tagId, String child, String childITS, String venue, String gate, String floor, String block, String seat) {
        Name = name;
        this.ITSId = ITSId;
        TagId = tagId;
        Child = child;
        ChildITS = childITS;
        Venue = venue;
        Gate = gate;
        Floor = floor;
        Block = block;
        Seat = seat;
    }


}
