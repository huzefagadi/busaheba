package com.huzefa.busahebapassprinter;

/**
 * Created by mufaddalgulshan on 13/10/14.
 */
public interface PrintCallback {
    public void connectPrinter();

    public void onPrinted();
}
