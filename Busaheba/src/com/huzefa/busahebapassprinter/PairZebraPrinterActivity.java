package com.huzefa.busahebapassprinter;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.os.Looper;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.aem.api.AEMScrybeDevice;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.discovery.BluetoothDiscoverer;

import java.util.ArrayList;

/**
 * Created by mufaddalgulshan on 11/10/14.
 */
public class PairZebraPrinterActivity extends DiscoveryResultList {


    public static final String TAG = "PairPrinterFragment";
    private Button btnPairConnect;
    private Button btnLater;
    private Toast mToast = null;
    private AEMScrybeDevice mAEMDevice;
    private AlertDialog.Builder mDialog;
    private ArrayAdapter<String> mAdapter;
    private String mAEMPrinterName;
    private ArrayList<String> mAEMPrinterList;
    private Bundle mBundle;
    private BluetoothDiscoverer mBluetoothDiscoverer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                try {
                    BluetoothDiscoverer.findPrinters(PairZebraPrinterActivity.this, PairZebraPrinterActivity.this);
                } catch (ConnectionException e) {
                    showToast(e.getMessage());
                } finally {
                    Looper.myLooper().quit();
                }
            }
        }).start();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void showToast(String strMessage) {
        if (mToast == null) {
            mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        }
        mToast.setText(strMessage);
        mToast.show();
    }
}

