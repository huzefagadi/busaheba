package com.huzefa.busahebapassprinter;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aem.api.AEMPrinter;
import com.zebra.sdk.comm.BluetoothConnectionInsecure;
import com.zebra.sdk.comm.Connection;

import java.io.IOException;

/**
 * Created by mufaddalgulshan on 13/10/14.
 */
public class ZebraPrintFragment extends Fragment {

	private TextView lblName;
	private TextView lblChild;
	private TextView lblITS;
	private TextView lblVenue;
	private TextView lblFloor;
	private TextView lblBlock;
	private TextView lblSeat;
	private TextView lblGate;
	private TextView lblPrint;
	private DatabaseHandler db;
	private PrintCallback mPrintCallback;
	private AEMPrinter mPrinter;
	// private String mPrinterName;
	//
	private SharedPreferences mPref;
	private Pass objPass;

	//
	private static final String KEY_PREF_EVENT = "pref_event";
	private static final String KEY_PREF_CITY = "pref_city";
	private static final String KEY_PREF_HELPLINE = "pref_helpline";
	private static final String KEY_PREF_PROGRAM = "pref_program";
	private static final String KEY_PREF_MONTH = "pref_month";
	private static final String KEY_PREF_DAY = "pref_day";
	private static final String KEY_PREF_MODE = "pref_mode";
	private static final String KEY_PREF_PRINTER = "pref_printer";
	//
	private static final String SEPARATOR = "********************************";
	//
	private String mEvent, mCity, mProgram, mHelpline, mITS, mDay, mMonth,
			mMode;

	private Bundle mBundle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		db = new DatabaseHandler(this.getActivity());
		mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof PrintCallback) {
			mPrintCallback = (PrintCallback) activity;
		} else {
			throw new ClassCastException(activity.toString()
					+ " must implement PassPrinterCallBack");
		}
	}

	public static ZebraPrintFragment newInstance(Pass objPass) {
		ZebraPrintFragment mZebraPrintFragment = new ZebraPrintFragment();
		mZebraPrintFragment.setPass(objPass);
		return mZebraPrintFragment;
	}

	private void setPass(Pass objPass) {
		this.objPass = objPass;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.print_fragment, container, false);

		mBundle = this.getArguments();

		if (v != null) {

			lblITS = (TextView) v.findViewById(R.id.lblITS);
			lblName = (TextView) v.findViewById(R.id.lblName);
			lblGate = (TextView) v.findViewById(R.id.lblGate);
			lblPrint = (TextView) v.findViewById(R.id.lblPrint);
			lblVenue = (TextView) v.findViewById(R.id.lblVenue);
			lblFloor = (TextView) v.findViewById(R.id.lblFloor);
			lblBlock = (TextView) v.findViewById(R.id.lblBlock);
			lblSeat = (TextView) v.findViewById(R.id.lblSeat);
			lblChild = (TextView) v.findViewById(R.id.lblChild);

			lblName.setText(objPass.getName());
			lblPrint.setText("Pass has been allocated to " + objPass.getITSId());
			lblITS.setText(objPass.getITSId());
			if (!objPass.getChild().equals("0")) {
				lblChild.setVisibility(View.VISIBLE);
			} else {
				lblChild.setVisibility(View.GONE);
			}
			lblGate.setText(objPass.getGate().toUpperCase());
			lblBlock.setText("Block: " + objPass.getBlock().toUpperCase());
			lblFloor.setText("Floor: " + objPass.getFloor().toUpperCase());
			lblSeat.setText("Seat: " + objPass.getSeat().toUpperCase());
			lblVenue.setText(objPass.getVenue());

			mMode = mPref.getString(KEY_PREF_MODE, "Auto Print");
			if (mMode.equals("Auto Print")) {
				try {
					connectAndPrint();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return v;
	}

	private void connectAndPrint() throws IOException {

		mEvent = mPref.getString(KEY_PREF_EVENT, "Ashara Mubaraka 1436H");
		mCity = mPref.getString(KEY_PREF_CITY, "Surat");
		mHelpline = mPref.getString(KEY_PREF_HELPLINE, "");
		mProgram = mPref.getString(KEY_PREF_PROGRAM, "Vaaz");
		mDay = mPref.getString(KEY_PREF_DAY, "3");
		mMonth = mPref.getString(KEY_PREF_MONTH, "Muharram al-Haraam");

		printPassOnZebra();

	}

	private void printPassOnZebra() {
		new Thread(new Runnable() {
			public void run() {
				try {
					// Instantiate insecure connection for given Bluetooth&reg;
					// MAC Address.
					Connection thePrinterConn = new BluetoothConnectionInsecure(
							SettingsHelper.getBluetoothAddress(getActivity()));

					// Initialize
					Looper.prepare();

					// Open the connection - physical connection is established
					// here.
					thePrinterConn.open();

					String event = "! 0 300 300 700 1\r\n"
							+ "BOX 2 10 570 680 3" + "CENTER\r\n"
							+ "TEXT 4 0 0 40 " + mEvent + "\r\n"
							+ "TEXT 4 0 0 90 " + mCity + "\r\n"
							+ "TEXT 7 0 0 150 Helpline: " + mHelpline + "\r\n"
							+ "LINE 8 200 570 200 4\r\n" + "TEXT 7 0 0 220 "
							+ objPass.getName() + "\r\n"
							+ "TEXT 4 0 0 260 ITS No: " + objPass.getITSId()
							+ "\r\n";
					if (!objPass.getChild().equals("0")) {
						event = event + "TEXT 7 0 0 320 +1 Child: "
								+ objPass.getChildITS() + "\r\n";
					}
					event = event + "LINE 8 360 570 360 4\r\n"
							+ "TEXT 4 0 0 380 " + mDay + " " + mMonth + " - "
							+ mProgram + "\r\n" + "TEXT 4 0 0 430 "
							+ objPass.getVenue() + "\r\n" + "LEFT\r\n"
							+ "TEXT 7 0 100 500 GATE\r\n"
							+ "BOX 60 535 200 635 1" + "TEXT 5 3 90 540 "
							+ objPass.getGate() + "\r\n"
							+ "TEXT 4 0 220 530 Seat: " + objPass.getSeat()
							+ "\r\n" + "TEXT 7 0 220 580 Floor: "
							+ objPass.getFloor() + "\r\n"
							+ "TEXT 7 0 220 630 Block: \r\n"
							+ "TEXT 4 0 305 610" + objPass.getBlock() + "\r\n"
							+ "PCX 35 90 !<LOGO.PCX\r\n"
							+ "PCX 0 0 !<1.PCX\r\n" + "PCX 476 0 !<2.PCX\r\n"
							+ "PCX 0 590 !<3.PCX\r\n"
							+ "PCX 476 590 !<4.PCX\r\n" + "FORM\r\n"
							+ "PRINT\r\n";

					// Send the data to printer as a byte array.
					thePrinterConn.write(event.getBytes());

					// Make sure the data got to the printer before closing the
					// connection
					Thread.sleep(1000);
					db.insertInHistory(mITS, mDay, mMonth);
					mPrintCallback.onPrinted();
					// Close the insecure connection to release resources.
					thePrinterConn.close();

					Looper.myLooper().quit();
				} catch (Exception e)

				{
					// Handle communications error here.
					e.printStackTrace();
					mPrintCallback.connectPrinter();
				}
			}
		}

		).start();
	}
}