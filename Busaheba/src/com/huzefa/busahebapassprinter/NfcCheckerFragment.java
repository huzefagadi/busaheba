package com.huzefa.busahebapassprinter;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class NfcCheckerFragment extends Fragment {

    public static final String TAG = "NfcCheckerFragment";
    private Button btnNfc;
    private TextView lblNfc;
    private NfcCheckerCallback mNfcCheckerCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.nfc_fragment, container, false);
        if (v != null) {
            lblNfc = (TextView) v.findViewById(R.id.lblNfc);
            btnNfc = (Button) v.findViewById(R.id.btnNfc);

            btnNfc.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        btnNfc.setBackgroundColor(getResources().getColor(R.color.Button_Pressed));
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        btnNfc.setBackgroundColor(getResources().getColor(R.color.Button));
                    }
                    return false;
                }
            });

            btnNfc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnNfc.getText().toString().equals("Continue")) {
                        mNfcCheckerCallback.onNfcTurnedOn();
                    } else {
                        Intent mSetNFC = new Intent(Settings.ACTION_NFC_SETTINGS);
                        startActivity(mSetNFC);
                    }
                }
            });
        }
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof NfcCheckerCallback) {
            mNfcCheckerCallback = (NfcCheckerCallback) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement NfcCheckerCallback");
        }
    }

    public interface NfcCheckerCallback {
        public void onNfcTurnedOn();
    }

    @Override
    public void onResume() {
        super.onResume();
        NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this.getActivity());
        if (nfc != null && nfc.isEnabled()) {
            btnNfc.setText("Continue");
            lblNfc.setText("NFC turned on");
        }
    }
}
