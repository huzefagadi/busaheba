package com.huzefa.busahebapassprinter;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aem.api.AEMPrinter;
import com.aem.api.AEMScrybeDevice;

import java.io.IOException;

/**
 * Created by mufaddalgulshan on 15/09/14.
 */
public class AEMPrintFragment extends Fragment {


    private TextView lblName;
    private TextView lblChild;
    private TextView lblITS;
    private TextView lblVenue;
    private TextView lblFloor;
    private TextView lblBlock;
    private TextView lblSeat;
    private TextView lblGate;
    private TextView lblPrint;
    private DatabaseHandler db;
    private PrintCallback mPrintCallback;
    private AEMPrinter mPrinter;
    private String mPrinterName;

    private SharedPreferences mPref;
    private Pass objPass;
//    private AEMScrybeDevice mDevice;

    private static final String KEY_PREF_EVENT = "pref_event";
    private static final String KEY_PREF_CITY = "pref_city";
    private static final String KEY_PREF_HELPLINE = "pref_helpline";
    private static final String KEY_PREF_PROGRAM = "pref_program";
    private static final String KEY_PREF_MONTH = "pref_month";
    private static final String KEY_PREF_DAY = "pref_day";
    private static final String KEY_PREF_MODE = "pref_mode";

    private static final String SEPARATOR = "********************************";

    private String mEvent, mCity, mProgram,
            mHelpline, mITS, mDay, mMonth, mMode;

    private Bundle mBundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(this.getActivity());
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof PrintCallback) {
            mPrintCallback = (PrintCallback) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement PassPrinterCallBack");
        }
    }

    public static AEMPrintFragment newInstance(AEMScrybeDevice mDevice, Pass objPass) {
        AEMPrintFragment mAEMPrintFragment = new AEMPrintFragment();
        mAEMPrintFragment.getPrinter(mDevice);
        mAEMPrintFragment.setPass(objPass);
        return mAEMPrintFragment;
    }

    private void getPrinter(AEMScrybeDevice mDevice) {
        mPrinter = mDevice.getAemPrinter();
    }

    private void setPass(Pass objPass) {
        this.objPass = objPass;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.print_fragment, container, false);

        mBundle = this.getArguments();
        mPrinterName = mBundle.getString("PRINTER_NAME");

        if (v != null) {

            lblITS = (TextView) v.findViewById(R.id.lblITS);
            lblName = (TextView) v.findViewById(R.id.lblName);
            lblGate = (TextView) v.findViewById(R.id.lblGate);
            lblPrint = (TextView) v.findViewById(R.id.lblPrint);
            lblVenue = (TextView) v.findViewById(R.id.lblVenue);
            lblFloor = (TextView) v.findViewById(R.id.lblFloor);
            lblBlock = (TextView) v.findViewById(R.id.lblBlock);
            lblSeat = (TextView) v.findViewById(R.id.lblSeat);
            lblChild = (TextView) v.findViewById(R.id.lblChild);

            lblName.setText(objPass.getName());
            lblPrint.setText("Pass has been allocated to " + objPass.getITSId());
            lblITS.setText(objPass.getITSId());
            if (!objPass.getChild().equals("0")) {
                lblChild.setVisibility(View.VISIBLE);
            } else {
                lblChild.setVisibility(View.GONE);
            }
            lblGate.setText(objPass.getGate().toUpperCase());
            lblBlock.setText("Block: " + objPass.getBlock().toUpperCase());
            lblFloor.setText("Floor: " + objPass.getFloor().toUpperCase());
            lblSeat.setText("Seat: " + objPass.getSeat().toUpperCase());
            lblVenue.setText(objPass.getVenue());

            mMode = mPref.getString(KEY_PREF_MODE, "Auto Print");
            if (mMode.equals("Auto Print")) {
                connectAndPrint();
            }
        }

        return v;
    }

    private void connectAndPrint() {
        if (mBundle.getBoolean("CONNECTED")) {
            if (mPrinterName != null) {
                try {
                    printPass();
                    db.insertInHistory(mITS, mDay, mMonth);
                    mPrintCallback.onPrinted();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else {
                mPrintCallback.connectPrinter();
            }
        } else {
            mPrintCallback.connectPrinter();
        }
    }

    private void printPass() throws IOException {


        mEvent = mPref.getString(KEY_PREF_EVENT, "Ashara Mubaraka 1435H");
        mCity = mPref.getString(KEY_PREF_CITY, "");
        mHelpline = mPref.getString(KEY_PREF_HELPLINE, "");
        mProgram = mPref.getString(KEY_PREF_PROGRAM, "Vaaz");
        mDay = mPref.getString(KEY_PREF_DAY, "3");
        mMonth = mPref.getString(KEY_PREF_MONTH, "Muharram al-Haraam");

        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.logo);

        mPrinter.printImage(b, getActivity(), AEMPrinter.IMAGE_CENTER_ALIGNMENT);

        mPrinter.setFontSize(AEMPrinter.DOUBLE_HEIGHT);
        mPrinter.print(mEvent, AEMPrinter.TEXT_ALIGNMENT_CENTER);
        mPrinter.print(mCity, AEMPrinter.TEXT_ALIGNMENT_CENTER);
        mPrinter.print("Ladies " + mProgram + " Pass", AEMPrinter.TEXT_ALIGNMENT_CENTER);
        if (!mHelpline.equals("")) {
            mPrinter.print("Helpline No: " + mHelpline, AEMPrinter.TEXT_ALIGNMENT_CENTER);
        }

        mPrinter.print(SEPARATOR, AEMPrinter.TEXT_ALIGNMENT_CENTER);

        mPrinter.print(objPass.getName(), AEMPrinter.TEXT_ALIGNMENT_CENTER);
        mPrinter.print("ITS No: " + objPass.getITSId(), AEMPrinter.TEXT_ALIGNMENT_CENTER);
        if (!objPass.getChild().equals("0")) {
            mPrinter.print("+1 Child: " + objPass.getChildITS(), AEMPrinter.TEXT_ALIGNMENT_CENTER);
        }
        mPrinter.print(SEPARATOR, AEMPrinter.TEXT_ALIGNMENT_CENTER);

        mPrinter.setFontSize(AEMPrinter.DOUBLE_HEIGHT);
        mPrinter.print(mDay + " " + mMonth, AEMPrinter.TEXT_ALIGNMENT_CENTER);

        mPrinter.print(SEPARATOR, AEMPrinter.TEXT_ALIGNMENT_CENTER);

        mPrinter.print(objPass.getVenue(), AEMPrinter.TEXT_ALIGNMENT_CENTER);
        mPrinter.setFontSize(AEMPrinter.DOUBLE_HEIGHT);
        mPrinter.print("GATE: " + objPass.getGate(), AEMPrinter.TEXT_ALIGNMENT_LEFT);
        mPrinter.print("Floor: " + objPass.getFloor(), AEMPrinter.TEXT_ALIGNMENT_RIGHT);
        mPrinter.print("Block: " + objPass.getBlock(), AEMPrinter.TEXT_ALIGNMENT_RIGHT);
        mPrinter.print("Seat: " + objPass.getSeat(), AEMPrinter.TEXT_ALIGNMENT_RIGHT);
        mPrinter.setLineFeed(4);
    }
}
