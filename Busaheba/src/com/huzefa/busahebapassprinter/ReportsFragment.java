package com.huzefa.busahebapassprinter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by mufaddalgulshan on 20/09/14.
 */
public class ReportsFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        DatabaseHandler db = new DatabaseHandler(getActivity());

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.reports_fragment, container, false);
        if (v != null) {

            TextView lblReport = (TextView) v.findViewById(R.id.lblReport);
            long mPrintedPasses = db.getPrintedPassesCount();
            lblReport.setText(mPrintedPasses + " passes have been printed using this app");
        }
        return v;
    }

}
