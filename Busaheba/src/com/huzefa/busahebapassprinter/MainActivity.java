package com.huzefa.busahebapassprinter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.aem.api.AEMScrybeDevice;
import com.aem.api.IAemScrybe;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements
		ImportFragment.ImportCallback, NfcCheckerFragment.NfcCheckerCallback,
		PairPrinterFragment.PairPrinterCallback,
		CardReaderFragment.PassCallback, PrintCallback {

	SimpleDateFormat sm = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	// region class variables
	private static final String KEY_PREF_EVENT = "pref_event";
	private static final String KEY_PREF_CITY = "pref_city";
	private static final String KEY_PREF_MODE = "pref_mode";
	public static final String MIME_TEXT_PLAIN = "text/plain";

	private ImportFragment mImportFragment;
	private CardReaderFragment mCardReaderFragment;
	private NfcCheckerFragment mNfcCheckerFragment;
	private PairPrinterFragment mPairPrinterFragment;

	private DatabaseHandler db;

	private AEMScrybeDevice mDevice;

	private NfcAdapter mNfcAdapter;

	private TextView lblEvent;
	private TextView lblCity;

	private String mAEMPrinterName;
	private String mMode;
	private boolean mIsPrinterPaired = false;
	private boolean mIsPrinterConnected = false;

	// endregion class variables

	// region Activity Life cycle
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (true) {
			mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

			PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
			loadPreferences();
			db = new DatabaseHandler(this);

			mDevice = new AEMScrybeDevice(new IAemScrybe() {
				@Override
				public void onDiscoveryComplete(ArrayList<String> strings) {

				}
			});

			onTagDiscovered(getIntent());
			updateFragment();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		/**
		 * It's important, that the activity is in the foreground (resumed).
		 * Otherwise an IllegalStateException is thrown.
		 */
		if (mNfcAdapter != null) {
			setupForegroundDispatch(this, mNfcAdapter);
		}
	}

	@Override
	protected void onPause() {
		/**
		 * Call this before onPause, otherwise an IllegalArgumentException is
		 * thrown as well.
		 */
		if (mNfcAdapter != null) {
			stopForegroundDispatch(this, mNfcAdapter);
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if (mDevice != null) {
			try {
				mDevice.disConnectPrinter();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// Check which request we're responding to
		if (requestCode == 1) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {
				if (data.hasExtra("PAIRED")) {
					SettingsHelper.saveBluetoothAddress(this,
							data.getStringExtra("PAIRED"));
					mIsPrinterPaired = true;
					UIHelper.showToast("MAC saved", this);
					UIHelper.showCardReaderFragment(this, mCardReaderFragment);
				}
			}
		}
	}

	// endregion Activity Life cycle

	// region Options Menu
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {

		case R.id.action_delete_all:
			deleteAllRecords();
			return true;

		case R.id.action_delete_history:
			deleteHistory();
			// db.deleteHistory();
			return true;

		case R.id.action_export_indata:
			exportINData();
			return true;

		case R.id.action_export_tagid_indata:
			exportTagIdINData();
			return true;

		case R.id.action_report:
			UIHelper.showRecordsFragment(this);
			return true;

		case R.id.action_settings:
			UIHelper.showSettingsFragment(this);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void exportINData() {
		// TODO Auto-generated method stub
		new ExportData().execute(1);

	}

	private void exportTagIdINData() {
		// TODO Auto-generated method stub
		new ExportData().execute(2);

	}

	private void deleteHistory() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					// Yes button clicked
					if (db.deleteHistory() > 0) {
						UIHelper.showToast("Printed passes history deleted",
								getApplicationContext());
						updateFragment();

					} else {
						UIHelper.showToast("Passes haven't been printed",
								getApplicationContext());
					}
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					dialog.dismiss();
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				"The history of printed passes will be deleted. Do you wish to continue?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();
	}

	private void deleteAllRecords() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					// Yes button clicked
					if (db.deletePasses() > 0) {
						UIHelper.showToast("Imported passes deleted",
								getApplicationContext());
						updateFragment();

					} else {
						UIHelper.showToast("Passes haven't been imported",
								getApplicationContext());
					}
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					dialog.dismiss();
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				"Imported passes will be deleted. Do you wish to continue?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();

	}

	// endregion Options Menu

	// region NFC reader
	@SuppressLint("NewApi")
	private boolean isNfcEnabled() {
		NfcAdapter nfc = NfcAdapter.getDefaultAdapter(this);
		if (nfc != null && nfc.isEnabled()) {
			return true;
		} else {
			return false;
		}
	}

	private void onTagDiscovered(Intent intent) {
		String action = intent.getAction();
		if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {

			// In case we would still use the Tech Discovered Intent
			Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			new GetMemberDetailsByTagId().execute(tag);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		/**
		 * This method gets called, when a new Intent gets associated with the
		 * current activity instance. Instead of creating a new activity,
		 * onNewIntent will be called. For more information have a look at the
		 * documentation.
		 * 
		 * In our case this method gets called, when the user attaches a Tag to
		 * the device.
		 */
		onTagDiscovered(intent);
	}

	/**
	 * @param activity
	 *            The corresponding {@link Activity} requesting the foreground
	 *            dispatch.
	 * @param adapter
	 *            The {@link NfcAdapter} used for the foreground dispatch.
	 */
	@SuppressLint("NewApi")
	public static void setupForegroundDispatch(final Activity activity,
			NfcAdapter adapter) {
		final Intent intent = new Intent(activity.getApplicationContext(),
				activity.getClass());
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

		final PendingIntent pendingIntent = PendingIntent.getActivity(
				activity.getApplicationContext(), 0, intent, 0);

		IntentFilter[] filters = new IntentFilter[1];
		String[][] techList = new String[][] {};

		// Notice that this is the same filter as in our manifest.
		filters[0] = new IntentFilter();
		filters[0].addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
		filters[0].addCategory(Intent.CATEGORY_DEFAULT);
		try {
			filters[0].addDataType(MIME_TEXT_PLAIN);
		} catch (IntentFilter.MalformedMimeTypeException e) {
			throw new RuntimeException("Check your mime type.");
		}

		adapter.enableForegroundDispatch(activity, pendingIntent, filters,
				techList);
	}

	/**
	 * @param activity
	 *            The corresponding {@link BaseActivity} requesting to stop the
	 *            foreground dispatch.
	 * @param adapter
	 *            The {@link NfcAdapter} used for the foreground dispatch.
	 */
	@SuppressLint("NewApi")
	public static void stopForegroundDispatch(final Activity activity,
			NfcAdapter adapter) {
		adapter.disableForegroundDispatch(activity);
	}

	@SuppressLint("NewApi")
	public static String ByteArrayToHexString(byte[] bytes) {
		final char[] hexArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
				'9', 'A', 'B', 'C', 'D', 'E', 'F' };
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	private class ExportData extends AsyncTask<Integer, Long, Long> {
		protected Long doInBackground(Integer... urls) {
			long total = 0;
			if (urls[0] == 1) {
				total = db.exportInData();
			} else {
				total = db.exportTagIdInData();
			}
			return total;
		}

		protected void onPostExecute(Long result) {
			if (result > 0) {
				Toast.makeText(getApplicationContext(),
						result + " Data Exported Successfully",
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(), "No Data Exported",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	public class GetMemberDetailsByTagId extends AsyncTask<Tag, Void, String> {
		Cursor mCursor;

		@SuppressLint("NewApi")
		@Override
		protected String doInBackground(Tag... params) {
			Tag tag = params[0];
			String mTagId = ByteArrayToHexString(tag.getId());
			mTagId = mTagId.toUpperCase();
			mCursor = db.getMemberDetailsByTagId(mTagId);

			int count = Integer.parseInt(String.valueOf(mCursor.getCount()));
			if (count > 0) {
				writeFileForINCsv(mCursor.getString(1));
			} else {
				writeFileForTagIdINCsv(mTagId);
			}

			return String.valueOf(mCursor.getCount());

		}

		@Override
		protected void onPostExecute(String result) {
			if (Integer.parseInt(result) != 0) {
				Pass objPass = new Pass(mCursor.getString(0) // NAME
						, mCursor.getString(1) // ITS_ID
						, mCursor.getString(2) // TAG_ID
						, mCursor.getString(3) // CHILD
						, mCursor.getString(4) // CHILD_ITS
						, mCursor.getString(5) // VENUE
						, mCursor.getString(6) // GATE
						, mCursor.getString(7) // FLOOR
						, mCursor.getString(8) // BLOCK
						, mCursor.getString(9)); // SEAT
				onPassReceived(objPass);
			} else {
				onInvalidPass();
			}
		}
	}

	// endregion NFC reader
	public void writeFileForINCsv(String ITSId) {
		db.insertINData(ITSId, "IN", sm.format(new Date()));
	}

	public void writeFileForTagIdINCsv(String tagId) {
		db.insertTagIdINData(tagId, "IN", sm.format(new Date()));
	}

	// region Interface Callbacks

	// region ImportCallback
	@Override
	public void onImportCompleted() {
		updateFragment();
	}

	// endregion ImportCallback

	// region NFCCheckerCallback
	@Override
	public void onNfcTurnedOn() {
		if (isAEMPrinterPaired()) {
			UIHelper.showCardReaderFragment(this, mCardReaderFragment);
		} else {
			UIHelper.showPairPrinterFragment(this, mPairPrinterFragment, "Pair");
		}
	}

	// endregion NFCCheckerCallback

	// region PairPrinterCallback
	@Override
	public void onPrinterConnected(String mPrinterName, AEMScrybeDevice mDevice) {
		this.mAEMPrinterName = mPrinterName;
		this.mDevice = mDevice;
		mIsPrinterConnected = true;
		mIsPrinterPaired = true;
		UIHelper.showCardReaderFragment(this, mCardReaderFragment);
	}

	@Override
	public void onPrinterPaired(String zebraBluetoothAddress) {
		mIsPrinterPaired = true;
		if (SettingsHelper.getPrinterSetting(this).equals("Zebra")) {
			SettingsHelper.saveBluetoothAddress(this, zebraBluetoothAddress);
		}
		UIHelper.showCardReaderFragment(this, mCardReaderFragment);
	}

	@Override
	public void onLater() {
		UIHelper.showCardReaderFragment(this, mCardReaderFragment);
	}

	// endregion PairPrinterCallback

	// region PassCallback
	@Override
	public void onPassReceived(Pass objPass) {

		SharedPreferences mPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		mMode = mPref.getString(KEY_PREF_MODE, "Auto Print");
		if (mMode.equals("Auto Print")) {

			// ZEBRA
			if (SettingsHelper.getPrinterSetting(this).equals("Zebra")) {
				if (// SettingsHelper.getBluetoothAddress(this) != ""
				mIsPrinterPaired) {
					UIHelper.showZebraPrintFragment(this, objPass, false,
							mIsPrinterConnected, mAEMPrinterName);
				} else {
					UIHelper.showPairPrinterFragment(this,
							mPairPrinterFragment, "Pair");
				}
			}

			// AEM
			else if (SettingsHelper.getPrinterSetting(this).equals("AEM")) {
				if (!mIsPrinterPaired) {
					UIHelper.showPairPrinterFragment(this,
							mPairPrinterFragment, "Pair");
				} else if (!mIsPrinterConnected) {
					UIHelper.showPairPrinterFragment(this,
							mPairPrinterFragment, "Connect");
				} else {
					UIHelper.showAEMPrintFragment(this, objPass, false,
							mDevice, mIsPrinterConnected, mAEMPrinterName);
				}
			}
		} else {
			UIHelper.showAEMPrintFragment(this, objPass, true, mDevice,
					mIsPrinterConnected, mAEMPrinterName);
		}
	}

	@Override
	public void onInvalidPass() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				UIHelper.showToast(
						"No pass has been allocated to this member.",
						getApplicationContext());
			}
		});
	}

	// endregion PassCallback

	// region PrintCallback
	@Override
	public void connectPrinter() {
		mIsPrinterConnected = false;
		UIHelper.showPairPrinterFragment(this, mPairPrinterFragment, "Connect");
	}

	@Override
	public void onPrinted() {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				UIHelper.showToast("Pass printed successfully.",
						getApplicationContext());
			}
		});
		UIHelper.showCardReaderFragment(this, mCardReaderFragment);
	}

	// endregion PrintCallback

	// endregion Interface Callbacks

	// region UI Management
	private void updateFragment() {
		if (arePassesImported()) {
			if (isNfcEnabled()) {
				String action = getIntent().getAction();
				if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
					onTagDiscovered(getIntent());
				} else {

					// AEM
					if (SettingsHelper.getPrinterSetting(this).equals("AEM")) {
						// Scan
						if (SettingsHelper.getMode(this).equals("Scan")) {
							UIHelper.showCardReaderFragment(this,
									mCardReaderFragment);
						}
						// Auto-Print
						else {
							// Check for printer paired
							if (isAEMPrinterPaired()) {
								UIHelper.showCardReaderFragment(this,
										mCardReaderFragment);
							} else {
								UIHelper.showPairPrinterFragment(this,
										mPairPrinterFragment, "Pair");
							}
						}

						// ZEBRA
					} else if (SettingsHelper.getPrinterSetting(this).equals(
							"Zebra")) {
						// Scan
						if (SettingsHelper.getMode(this).equals("Scan")) {
							UIHelper.showCardReaderFragment(this,
									mCardReaderFragment);
						}
						// Auto-Print
						else {
							if (isZebraPrinterPaired()) {
								UIHelper.showCardReaderFragment(this,
										mCardReaderFragment);
							} else {
								UIHelper.showPairPrinterFragment(this,
										mPairPrinterFragment, "Pair");
							}
						}
					}

				}
			} else {
				UIHelper.showNFCCheckerFragment(this, mNfcCheckerFragment);
			}
		} else {
			UIHelper.showImportFragment(this, mImportFragment);
		}
	}

	private boolean isZebraPrinterPaired() {
		String mBluetoothAddress = SettingsHelper.getBluetoothAddress(this);
		if (mBluetoothAddress != "") {
			return true;
		} else {
			return false;
		}
	}

	private boolean arePassesImported() {
		return db.rowExists();
	}

	private boolean isAEMPrinterPaired() {
		SharedPreferences mPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		mMode = mPref.getString(KEY_PREF_MODE, "Auto Print");
		if (mMode.equals("Auto Print")) {
			ArrayList<String> mPairedPrintersList = mDevice.getPairedPrinters();
			if (mPairedPrintersList.size() != 0) {
				mIsPrinterPaired = true;
			} else {
				mIsPrinterPaired = false;
			}
			return mIsPrinterPaired;
		} else {
			return true;
		}
	}

	// endregion UI Management

	public void loadPreferences() {
		SharedPreferences mPref = PreferenceManager
				.getDefaultSharedPreferences(this);
		lblEvent = (TextView) findViewById(R.id.lblEvent);
		lblEvent.setText(mPref.getString(KEY_PREF_EVENT,
				"Ashara Mubaraka 1436H"));

		lblCity = (TextView) findViewById(R.id.lblCity);
		lblCity.setText(mPref.getString(KEY_PREF_CITY, "Surat"));
	}
}
