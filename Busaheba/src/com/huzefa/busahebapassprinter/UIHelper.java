package com.huzefa.busahebapassprinter;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.aem.api.AEMScrybeDevice;

/**
 * Created by mufaddalgulshan on 18/10/14.
 */
public class UIHelper {
    private static Toast mToast = null;

    public static void showImportFragment(Activity activity, ImportFragment mImportFragment) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        mImportFragment = new ImportFragment();
        transaction.replace(R.id.sample_content_fragment, mImportFragment);
        transaction.commit();
    }

    public static void showNFCCheckerFragment(Activity activity, NfcCheckerFragment mNfcCheckerFragment) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        mNfcCheckerFragment = new NfcCheckerFragment();
        transaction.replace(R.id.sample_content_fragment, mNfcCheckerFragment);
        transaction.commit();
    }

    public static void showCardReaderFragment(Activity activity, CardReaderFragment mCardReaderFragment) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        mCardReaderFragment = new CardReaderFragment();
        transaction.replace(R.id.sample_content_fragment, mCardReaderFragment);
        transaction.commit();
    }

    public static void showPairPrinterFragment(Activity activity, PairPrinterFragment mPairPrinterFragment
            , String mAction) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        mPairPrinterFragment = new PairPrinterFragment();
        Bundle mBundle = new Bundle();
        mBundle.putString("ACTION", mAction);
        mPairPrinterFragment.setArguments(mBundle);
        transaction.replace(R.id.sample_content_fragment, mPairPrinterFragment);
        transaction.commit();
    }

    public static void showSettingsFragment(Activity activity) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        SettingsFragment mSettingsFragment = new SettingsFragment();
        transaction.addToBackStack(null);
        transaction.replace(R.id.sample_content_fragment, mSettingsFragment);
        transaction.commit();
    }

    public static void showRecordsFragment(Activity activity) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        ReportsFragment mReportsFragment = new ReportsFragment();
        transaction.addToBackStack(null);
        transaction.replace(R.id.sample_content_fragment, mReportsFragment);
        transaction.commit();
    }

    public static void showAEMPrintFragment(Activity activity
            , Pass objPass, boolean mAddToBackStack
            , AEMScrybeDevice mDevice, boolean mIsPrinterConnected, String mAEMPrinterName) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        AEMPrintFragment mAEMPrintFragment = AEMPrintFragment.newInstance(mDevice, objPass);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("CONNECTED", mIsPrinterConnected);
        mBundle.putString("PRINTER_NAME", mAEMPrinterName);
        mAEMPrintFragment.setArguments(mBundle);
        transaction.replace(R.id.sample_content_fragment, mAEMPrintFragment);
        if (mAddToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void showZebraPrintFragment(Activity activity
            , Pass objPass, boolean mAddToBackStack
            , boolean mIsPrinterConnected, String mAEMPrinterName) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        ZebraPrintFragment mZebraPrintFragment = ZebraPrintFragment.newInstance(objPass);
        Bundle mBundle = new Bundle();
        mBundle.putBoolean("CONNECTED", mIsPrinterConnected);
        mBundle.putString("PRINTER_NAME", mAEMPrinterName);
        mZebraPrintFragment.setArguments(mBundle);
        transaction.replace(R.id.sample_content_fragment, mZebraPrintFragment);
        if (mAddToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void showToast(String strMessage, Context context) {
        if (mToast == null) {
            mToast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
        }
        mToast.setText(strMessage);
        mToast.show();
    }
}
