package com.huzefa.busahebapassprinter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


@SuppressLint("NewApi") 
public class CardReaderFragment extends Fragment {
	SimpleDateFormat sm = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    private DatabaseHandler db;
    private Button btnSearch;
    private EditText txtITS;
	
    private PassCallback mPassCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(this.getActivity());
    }

    public interface PassCallback {
        public void onPassReceived(Pass objPass);

        public void onInvalidPass();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.card_reader_fragment, container, false);
        if (v != null) {

            txtITS = (EditText) v.findViewById(R.id.txtITS);

            btnSearch = (Button) v.findViewById(R.id.btnSearch);
            btnSearch.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("NewApi") @Override
                public void onClick(View view) {

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(txtITS.getWindowToken(), 0);

                    Cursor mCursor = db.getPassDetailsByITS(txtITS.getText().toString());
                    
                    if(mCursor.getCount() != 0)
                    {
                    	writeFileForINCsv(mCursor.getString(1));
                    	System.out.println("CALLED HUZEFA");
                    }
                    if (mCursor.getCount() != 0) {
                        Pass objPass = new Pass(
                                mCursor.getString(0)        //NAME
                                , mCursor.getString(1)      //ITS_ID
                                , mCursor.getString(2)      //TAG_ID
                                , mCursor.getString(3)      //CHILD
                                , mCursor.getString(4)      //CHILD_ITS
                                , mCursor.getString(5)      //VENUE
                                , mCursor.getString(6)      //GATE
                                , mCursor.getString(7)      //FLOOR
                                , mCursor.getString(8)      //BLOCK
                                , mCursor.getString(9));    //SEAT
                        mPassCallback.onPassReceived(objPass);
                    } else {
                        mPassCallback.onInvalidPass();
                    }
                }
            });
        }
        return v;
    }
    public void writeFileForINCsv(String ITSId)
	{
    	db.insertINData(ITSId, "IN", sm.format(new Date()));
	}
    @SuppressLint("NewApi") @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof PassCallback) {
            mPassCallback = (PassCallback) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement MemberCallback");
        }
    }
}
