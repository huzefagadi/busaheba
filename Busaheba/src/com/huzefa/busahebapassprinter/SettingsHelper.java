/***********************************************
 * CONFIDENTIAL AND PROPRIETARY 
 *
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published, 
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 *
 * Copyright ZIH Corp. 2012
 *
 * ALL RIGHTS RESERVED
 ***********************************************/

package com.huzefa.busahebapassprinter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SettingsHelper {

    private static final String PREFS_NAME = "OurSavedAddress";
    private static final String bluetoothAddressKey = "ZEBRA_BLUETOOTH_ADDRESS";
    private static final String KEY_PREF_EVENT = "pref_event";
    private static final String KEY_PREF_CITY = "pref_city";
    private static final String KEY_PREF_HELPLINE = "pref_helpline";
    private static final String KEY_PREF_PROGRAM = "pref_program";
    private static final String KEY_PREF_DAY = "pref_day";
    private static final String KEY_PREF_MONTH = "pref_month";
    private static final String KEY_PREF_MODE = "pref_mode";
    private static final String KEY_PREF_PRINTER = "pref_printer";

    public static String getBluetoothAddress(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(bluetoothAddressKey, "");
    }

    public static void saveBluetoothAddress(Context context, String address) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(bluetoothAddressKey, address);
        editor.commit();
    }

    public static String getPrinterSetting(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(KEY_PREF_PRINTER, "Zebra");
    }

    public static String getMode(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString(KEY_PREF_MODE, "Auto-Print");
    }
}
