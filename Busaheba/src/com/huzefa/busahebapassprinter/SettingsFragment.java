package com.huzefa.busahebapassprinter;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.TextView;

/**
 * Created by mufaddalgulshan on 20/09/14.
 */
public class SettingsFragment extends PreferenceFragment {

    private static final String KEY_PREF_EVENT = "pref_event";
    private static final String KEY_PREF_CITY = "pref_city";
    private static final String KEY_PREF_HELPLINE = "pref_helpline";
    private static final String KEY_PREF_PROGRAM = "pref_program";
    private static final String KEY_PREF_DAY = "pref_day";
    private static final String KEY_PREF_MONTH = "pref_month";
    private static final String KEY_PREF_MODE = "pref_mode";
    private static final String KEY_PREF_PRINTER = "pref_printer";

    private SharedPreferences mPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        setPrefSummary();

        final TextView lblEvent = (TextView) getActivity().findViewById(R.id.lblEvent);
        final TextView lblCity = (TextView) getActivity().findViewById(R.id.lblCity);

        SharedPreferences.OnSharedPreferenceChangeListener listener =
                new SharedPreferences.OnSharedPreferenceChangeListener() {
                    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                        // listener implementation
                        Preference connectionPref = findPreference(key);
                        // Set summary to be the user-description for the selected value
                        connectionPref.setSummary(prefs.getString(key, ""));

                        if (key.equals(KEY_PREF_EVENT)) {
                            lblEvent.setText(prefs.getString(key, ""));
                        } else if (key.equals(KEY_PREF_CITY)) {
                            lblCity.setText(prefs.getString(key, ""));
                        } else if (key.equals(KEY_PREF_HELPLINE)) {
                            lblCity.setText(prefs.getString(key, ""));
                        } else if (key.equals(KEY_PREF_PRINTER)) {
                            lblCity.setText(prefs.getString(key, ""));
                        }
                    }
                };

        mPref.registerOnSharedPreferenceChangeListener(listener);
    }

    private void setPrefSummary() {
        Preference connectionPref = findPreference(KEY_PREF_EVENT);
        connectionPref.setSummary(mPref.getString(KEY_PREF_EVENT, "Ashara Mubaraka 1436H"));

        connectionPref = findPreference(KEY_PREF_CITY);
        connectionPref.setSummary(mPref.getString(KEY_PREF_CITY, "Surat"));

        connectionPref = findPreference(KEY_PREF_HELPLINE);
        connectionPref.setSummary(mPref.getString(KEY_PREF_HELPLINE, ""));

        connectionPref = findPreference(KEY_PREF_DAY);
        connectionPref.setSummary(mPref.getString(KEY_PREF_DAY, "2"));

        connectionPref = findPreference(KEY_PREF_MONTH);
        connectionPref.setSummary(mPref.getString(KEY_PREF_MONTH, "Muharram"));

        connectionPref = findPreference(KEY_PREF_PROGRAM);
        connectionPref.setSummary(mPref.getString(KEY_PREF_PROGRAM, "Waaz"));

        connectionPref = findPreference(KEY_PREF_MODE);
        connectionPref.setSummary(mPref.getString(KEY_PREF_MODE, "Auto Print"));

        connectionPref = findPreference(KEY_PREF_PRINTER);
        connectionPref.setSummary(mPref.getString(KEY_PREF_PRINTER, "Zebra"));
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
