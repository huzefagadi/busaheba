package com.huzefa.busahebapassprinter;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * Created by mufaddalgulshan on 14/09/14.
 */
public class ImportFragment extends Fragment {

    private DatabaseHandler db;

    private ImportCallback mImportCallback;

    private Button btnImport;
    private TextView lblImport;
    private ProgressBar progressBar;
    private int iRows = 0;
    String mDecryptedCSV;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(this.getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.import_fragment, container, false);
        if (v != null) {
            btnImport = (Button) v.findViewById(R.id.btnImport);
            lblImport = (TextView) v.findViewById(R.id.lblImport);
            progressBar = (ProgressBar) v.findViewById(R.id.progressbar_Horizontal);
            progressBar.setVisibility(View.INVISIBLE);

            btnImport.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        btnImport.setBackgroundColor(getResources().getColor(R.color.Button_Pressed));
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        btnImport.setBackgroundColor(getResources().getColor(R.color.Button));
                    }
                    return false;
                }
            });

            btnImport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Continue
                    if (btnImport.getText().toString().equals("Continue")) {
                        mImportCallback.onImportCompleted();
                    }

                    //Import
                    else {

                        //Open CSV
                        OpenFileDialog mFileDialog = new OpenFileDialog(getActivity());
                        mFileDialog.setTitle("Choose a .csv file to import");
                        mFileDialog.setFilter(".csv");
                        mFileDialog.show();

                        mFileDialog.setOpenDialogListener(
                                new OpenFileDialog.OpenDialogListener() {

                                    @Override
                                    public void OnSelectedFile(String fileName) {
                                        DecryptionUtil mDecryptionUtil = new DecryptionUtil();
                                        String mEncryptedCSV = mDecryptionUtil.getEncryptedCSV(fileName);
                                        try {
                                            mDecryptedCSV = mDecryptionUtil.decrypt(mEncryptedCSV);
                                        } catch (Exception e) {
                                            UIHelper.showToast("Couldn't decrpyt the file", getActivity());
                                        }
                                        System.out.println(mDecryptedCSV);
                                        importItems(mDecryptedCSV);
                                    }
                                });
                    }
                }
            });
        }
        return v;
    }


    public interface ImportCallback {
        public void onImportCompleted();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof ImportCallback) {
            mImportCallback = (ImportCallback) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement ImportFragment.ImportCallback");
        }
    }

    private void importItems(String mDecryptedCSV) {
        if (validateColumns(mDecryptedCSV)) {
            iRows = getRowCount(mDecryptedCSV);
            if (iRows > 0) {
                db.deletePasses();
                new BackgroundAsyncTask().execute();
                btnImport.setClickable(false);
            }
        } else {
            UIHelper.showToast("The order of columns in the selected csv file is incorrect", getActivity());
        }
    }

    private boolean validateColumns(String mDecryptedCSV) {
        String line;
        boolean mIsValid = true;
        BufferedReader reader = new BufferedReader(
                new StringReader(mDecryptedCSV));
        try {
            line = reader.readLine();
            String[] columns = line.split(",");
            if (!columns[0].toUpperCase().equals("NAME")
                    || !columns[1].toUpperCase().equals("ITS_ID")
                    || !columns[2].toUpperCase().equals("TAG_ID")
                    || !columns[3].toUpperCase().equals("CHILD")
                    || !columns[4].toUpperCase().equals("CHILD_ITS")
                    || !columns[5].toUpperCase().equals("VENUE")
                    || !columns[6].toUpperCase().equals("GATE")
                    || !columns[7].toUpperCase().equals("FLOOR")
                    || !columns[8].toUpperCase().equals("BLOCK")
                    || !columns[9].toUpperCase().equals("SEAT")) {
                mIsValid = false;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return mIsValid;
    }

    private int getRowCount(String mDecryptedCSV) {
        String line;
        int iRows = 0;
        BufferedReader reader = new BufferedReader(
                new StringReader(mDecryptedCSV));
        try {
            line = reader.readLine();
            while ((line = reader.readLine()) != null) {
                iRows++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return iRows;
    }

    public class BackgroundAsyncTask extends AsyncTask<Void, Integer, Void> {

        int myProgressCount;

        @Override
        protected void onPreExecute() {

            lblImport.setText("Importing...This should take a few minutes...");

            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setDrawingCacheBackgroundColor(Color.parseColor("#ff382139"));
            myProgressCount = 0;
        }

        @Override
        protected Void doInBackground(Void... params) {
            String line = null;
            BufferedReader reader = new BufferedReader(
                    new StringReader(mDecryptedCSV));
            try {
                line = reader.readLine();
                while ((line = reader.readLine()) != null) {
                    myProgressCount++;
                    String[] insertValues = line.split(",");
                    db.insertItems(insertValues[0], insertValues[1], insertValues[2], insertValues[3], insertValues[4]
                            , insertValues[5], insertValues[6], insertValues[7], insertValues[8], insertValues[9]);
                    publishProgress(myProgressCount * 100 / iRows);
                }
                db.close();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // TODO Auto-generated method stub
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(Void result) {
            btnImport.setClickable(true);
            btnImport.setText("Continue");
            lblImport.setText("Passes imported");

            if (iRows > 0) {
                UIHelper.showToast(iRows + " Items imported!", getActivity());
            }
        }
    }
}
