/***********************************************
 * CONFIDENTIAL AND PROPRIETARY 
 *
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published, 
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 *
 * Copyright ZIH Corp. 2012
 *
 * ALL RIGHTS RESERVED
 ***********************************************/

package com.huzefa.busahebapassprinter;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.TwoLineListItem;

import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveredPrinterBluetooth;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;

import java.util.ArrayList;
import java.util.List;

public abstract class DiscoveryResultList extends ListActivity implements DiscoveryHandler {

    protected List<String> discoveredPrinters = null;
    private ZebraListAdapter mListAdapter;
    private Toast mToast = null;
    private DiscoveredPrinter mZebraPrinter;

    public DiscoveryResultList() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.discovery_results);

        setProgressBarIndeterminateVisibility(true);

        mListAdapter = new ZebraListAdapter();
        setListAdapter(mListAdapter);


    }

    @Override
    public void finish() {
        // Prepare data intent
        Intent data = new Intent();
        data.putExtra("PAIRED", mZebraPrinter.address);
        // Activity finished ok, return the data
        setResult(RESULT_OK, data);
        super.finish();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        mZebraPrinter = (DiscoveredPrinter) mListAdapter.getItem(position);
        showToast("Paired");
        finish();
    }

    public void foundPrinter(final DiscoveredPrinter printer) {
        runOnUiThread(new Runnable() {
            public void run() {
                mListAdapter.addPrinterItem((DiscoveredPrinter) printer);
                mListAdapter.notifyDataSetChanged();
            }
        });
    }

    public void discoveryFinished() {
        runOnUiThread(new Runnable() {
            public void run() {
//                Toast.makeText(DiscoveryResultList.this, " Discovered " + mListAdapter.getCount() + " printers", Toast.LENGTH_SHORT).show();
                setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    public void discoveryError(String message) {
        showToast(message);
    }

    private class ZebraListAdapter extends BaseAdapter {

        private ArrayList<DiscoveredPrinter> printerItems;

        public ZebraListAdapter() {
            printerItems = new ArrayList<DiscoveredPrinter>();
        }

        public void addPrinterItem(DiscoveredPrinter p) {
            printerItems.add(p);
        }

        @Override
        public int getCount() {
            return printerItems.size();
        }

        @Override
        public Object getItem(int groupPosition) {
            return printerItems.get(groupPosition);
        }

        @Override
        public long getItemId(int groupPosition) {
            return groupPosition;
        }

        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int groupPosition, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) DiscoveryResultList.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            TwoLineListItem itemView = (TwoLineListItem) (inflater.inflate(android.R.layout.simple_list_item_2, null));
            if (printerItems.get(groupPosition).getDiscoveryDataMap().containsKey("DARKNESS"))
                itemView.setBackgroundColor(0xff4477ff);
            if (printerItems.get(groupPosition) instanceof DiscoveredPrinterBluetooth) {
                itemView.getText1().setText(((DiscoveredPrinterBluetooth) printerItems.get(groupPosition)).friendlyName);
                itemView.getText2().setText(((DiscoveredPrinterBluetooth) printerItems.get(groupPosition)).address);
            }
            return itemView;
        }
    }

    private void showToast(String strMessage) {
        if (mToast == null) {
            mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        }
        mToast.setText(strMessage);
        mToast.show();
    }
}
